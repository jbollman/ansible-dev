#!/usr/bin/env bash

git clone https://gitlab.com/jbollman/ansible-dev.git
sudo apt update -y;
sudo apt-add-repository ppa:ansible/ansible
sudo apt update -y
sudo apt install ansible -y

ansible-playbook ansible-dev/csharp-install.yml
